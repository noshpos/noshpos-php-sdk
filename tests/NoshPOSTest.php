<?php

class NoshPOSTest extends \PHPUnit\Framework\TestCase {
    public function testClientIdCannotBeEmpty()
    {
        $clientId = '';
        $clientSecret = 'abc';
        $baseUrl = 'http://127.0.0.1';

        $this->expectException(Exception::class);
        $pos = new \NoshPOS\APIWrapper\NoshPOS($clientId, $clientSecret, $baseUrl);
    }

    public function testClientSecretCannotBeEmpty()
    {
        $clientId = '123';
        $clientSecret = '';
        $baseUrl = 'http://127.0.0.1';

        $this->expectException(Exception::class);
        $pos = new \NoshPOS\APIWrapper\NoshPOS($clientId, $clientSecret, $baseUrl);
    }

    public function testBaseUrlHasToBeValidUrl()
    {
        $clientId = '123';
        $clientSecret = 'abc';
        $baseUrl = '';

        $this->expectException(Exception::class);
        $pos = new \NoshPOS\APIWrapper\NoshPOS($clientId, $clientSecret, $baseUrl);
    }

    public function testGuzzleClientAuthHeaderSet()
    {
        $clientId = '123';
        $clientSecret = 'abc';
        $baseUrl = 'http://127.0.0.1';

        $pos = new \NoshPOS\APIWrapper\NoshPOS($clientId, $clientSecret, $baseUrl);

        $httpClient = $pos->getHttpClient();

        $headers = $httpClient->getConfig('headers');

        if(empty($headers['X-AUTH-TOKEN']))
            $this->fail('X-AUTH-TOKEN header not set in HTTP client.');
    }

    public function testRetrievingConfig()
    {
        $config = \NoshPOS\APIWrapper\NoshPOS::getConfig();

        if(!is_array($config))
            $this->fail('Returned config is not an array.');
    }
    
}