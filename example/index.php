<?php

namespace NoshPOS\APIWrapper;

require_once(__DIR__.'/../vendor/autoload.php');

$clientId = '';
$clientSecret = '';

$noshpos = new NoshPOS($clientId, $clientSecret);

$noshpos->setPageSize(1);
$noshpos->returnDataArray(); // Returning php data array is toggled on. ->get() will now return the array directly.

$response = $noshpos->customers()->get();

var_dump($response);