#NoshPOS API Wrapper (PHP)

This library wraps the NoshPOS API and allows you to retrieve paginated basic data from NoshPOS.

##Requirements
- A [NoshPOS](http://noshpos.cn) API client id and secret.
- [Composer](http://getcomposer.org)

##Installation

1. Add the wrapper to your composer.json and run `composer install`
2. Load the wrapper namespace in your code: `use NoshPOS\APIWrapper`
3. Instantiate the API wrapper: `$noshpos = new NoshPOS($clientId, $clientSecret, $url)`

##Usage
1. Instantiate a `NoshPOS` API wrapper object, pass along your client id and secret.
2. `$noshPOS->customers()->get($pageSize, $pageIndex)`. By passing page size 1 and index 0, the wrapper would return one customer.

##Example Implementation
See `example/` directory.

##Unit Tests
Run `vendor/bin/phpunit` on the `tests` directory to execute the test suite.

##Credits
First version of this wrapper written by [John Jensen](mailto:john@jenseninnovations.se),
with help from [Celso Fernandes](mailto:celso@thephphero.com).