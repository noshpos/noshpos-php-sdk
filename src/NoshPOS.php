<?php

namespace NoshPOS\APIWrapper;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\Yaml\Yaml;

/**
 * Class NoshPOS
 * @package NoshPOS\API
 */
class NoshPOS
{
    private $httpClient;
    private $returnDataArray;
    private $pageSize;

    /**
     * NoshPOS constructor.
     * @param $clientId string
     * @param $clientSecret string
     * @param $baseUrl string
     * @param $timeout int
     * @throws \Exception
     */
    public function __construct($clientId, $clientSecret, $baseUrl = null, $timeout = 0)
    {
        if(!$clientId)
            throw new \Exception('Client ID cannot be empty.');

        if(!$clientSecret)
            throw new \Exception('Client secret cannot be empty.');

        $config = self::getConfig();
        $baseUrl = (empty($baseUrl) ? $config['default_url'] : $baseUrl);

        if(!$baseUrl)
            throw new \Exception('Base URL must not be empty.');

        // Establish connection
        $httpClient = new Client([
            'base_uri' => $baseUrl,
            #'timeout' => $timeout,
            'headers' => [
                'X-AUTH-TOKEN' => $clientId.':'.$clientSecret
            ]
        ]);

        $this->httpClient = $httpClient;

        //
        $this->returnDataArray = false;
        $this->pageSize = $config['default_page_size'];
    }

    /**
     * Force GETs to return a PHP data array of the body, otherwise a Guzzle Response object will be returned.
     *
     * @param bool $returnDataArray
     */
    public function returnDataArray($returnDataArray = true) {
        $this->returnDataArray = $returnDataArray;
    }

    /**
     * Set the default page size that are returned by the API.
     *
     * @param $pageSize
     */
    public function setPageSize($pageSize) {
        $this->pageSize = $pageSize;
    }

    /**
     * Returns the default configuration parameters.
     *
     * @return mixed
     * @throws \Exception
     */
    public static function getConfig()
    {
        $path = __DIR__.'/../config/config.yml';

        if(!is_file($path))
            throw new \Exception('File could not be read.');

        $configuration = Yaml::parse(file_get_contents($path));

        return $configuration;
    }

    private $path;

    /**
     * Method for fetching data from the Noshpos API.
     *
     * @param int $pageIndex
     * @param array $parameters Any parameters you wish to pass to the API.
     * @return Response|array Directly return a PHP array with the resulting data. Will throw an Exception if there's an error communicating with the API.
     * @throws \Exception
     */
    public function get($parameters = [], $pageIndex = 0)
    {
        $config = self::getConfig();

        $params = [
            'limit' => $this->pageSize,
            'offset' => $pageIndex,
            'language_id' => $config['default_language_id']
        ];
        $params = array_merge($params, $parameters);

        $path = $this->path;
        $options = [
            'query' => $params
        ];

        $response = $this->httpClient->request('GET', $path, $options);

        if($this->returnDataArray) {
            if($response->getStatusCode() != 200)
                throw new \Exception('HTTP '.$response->getStatusCode().": ".$response->getReasonPhrase());

            $body = $response->getBody();
            return json_decode($body, true);
        }

        return $response;
    }

    /**
     * Method for POSTing to the Noshpos API.
     *
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function post($parameters = []) {
        $path = $this->path;

        $config = self::getConfig();

        $defaultParameters = [
            'language_id' => $config['default_language_id']
        ];

        $parameters = array_merge($defaultParameters, $parameters);

        $options = [
            'form_params' => $parameters
        ];

        $response = $this->httpClient->request('POST', $path, $options);

        if($this->returnDataArray) {
            if($response->getStatusCode() != 200)
                throw new \Exception('HTTP '.$response->getStatusCode().": ".$response->getReasonPhrase());

            $body = $response->getBody();
            return json_decode($body, true);
        }

        return $response;
    }

    /**
     * Method for PUTing to the Noshpos API.
     * @param $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function put($parameters){
        $path = $this->path;

        $config = self::getConfig();

        $defaultParameters = [
            'language_id' => $config['default_language_id']
        ];

        $parameters = array_merge($defaultParameters, $parameters);

        $options = [
            'form_params' => $parameters
        ];

        $response = $this->httpClient->request('PUT', $path, $options);

        if($this->returnDataArray) {
            if($response->getStatusCode() != 200)
                throw new \Exception('HTTP '.$response->getStatusCode().": ".$response->getReasonPhrase());

            $body = $response->getBody();
            return json_decode($body, true);
        }

        return $response;
    }

    /**
     * Method for DELETE to the Noshpos API.
     * @param $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     */
    public function delete($parameters=[]){
        $path = $this->path;

        $config = self::getConfig();

        $defaultParameters = [
            'language_id' => $config['default_language_id']
        ];

        $parameters = array_merge($defaultParameters, $parameters);

        $options = [
            'form_params' => $parameters
        ];

        $response = $this->httpClient->request('DELETE', $path, $options);

        if($this->returnDataArray) {
            if($response->getStatusCode() != 200)
                throw new \Exception('HTTP '.$response->getStatusCode().": ".$response->getReasonPhrase());

            $body = $response->getBody();
            return json_decode($body, true);
        }


        return $response;
    }

    public function customer($customerId)
    {
        $this->path = 'customers/'.$customerId;

        return $this;
    }

    public function customerLogin()
    {
        $this->path = 'customers/login';

        return $this;
    }

    public function customers()
    {
        $this->path = 'customers';

        return $this;
    }

    public function orders($customerId)
    {
        $this->path = 'orders/customer/'.$customerId;

        return $this;
    }

    public function order()
    {
        $this->path = 'orders';

        return $this;
    }

    public function getOrder($orderId)
    {
        $this->path = 'orders/'.$orderId;

        return $this;
    }

    public function settings()
    {
        $this->path = 'settings';

        return $this;
    }

    public function products()
    {
        $this->path = 'products';

        return $this;
    }

    public function product($productId)
    {
        $this->path = 'product/'.$productId;

        return $this;
    }

    public function categories()
    {
        $this->path = 'categories';

        return $this;
    }

    public function addresses($customerId)
    {
        $this->path = 'addresses/'.$customerId;

        return $this;
    }

    public function vouchers()
    {
        $this->path = 'vouchers';

        return $this;
    }

    public function validateVoucher($voucherId)
    {
        $this->path = 'vouchers/validate/'.$voucherId;

        return $this;
    }

    public function discounts()
    {
        $this->path = 'discounts';

        return $this;
    }

    public function discount($discountId)
    {
        $this->path = 'discounts/'.$discountId;

        return $this;
    }

    public function payments()
    {
        $this->path = 'payments';

        return $this;
    }

    public function deliveryMethods()
    {
        $this->path = 'shipping';

        return $this;
    }

    public function cart()
    {
        $this->path = 'cart';

        return $this;
    }

    public function addCartItem($cartId)
    {
        $this->path = 'cart/item/'.$cartId;

        return $this;
    }

    public function CartItem($cartItemId)
    {
        $this->path = 'cart/item/'.$cartItemId;

        return $this;
    }

    public function cartItems()
    {
        $this->path = 'cart/items';

        return $this;
    }

    public function addVoucher($cartId,$voucherId,$customerId)
    {
        $this->path = 'cart/voucher/'.$cartId.'/'.$voucherId.'/'.$customerId;

        return $this;
    }

    public function timeSlots()
    {
        $this->path = 'timeslots';

        return $this;
    }

    public function timeSlotExceptions()
    {
        $this->path = 'timeslotExceptions';

        return $this;
    }

    public function modifierGroups()
    {
        $this->path = 'modifiers';

        return $this;
    }

    public function getModifierGroup($modifierGroupId)
    {
        $this->path = 'modifiers/'.$modifierGroupId;

        return $this;
    }

    public function getProductModifierGroups($productId)
    {
        $this->path = 'modifiers/product/'.$productId;

        return $this;
    }

    public function shop()
    {
        $this->path = 'shop';

        return $this;
    }

    public function passwordReset()
    {
        $this->path = 'password/reset';

        return $this;
    }

    public function verifyPhone()
    {
        $this->path = 'customers/verify';

        return $this;
    }

    public function wechatTransaction()
    {
        $this->path = 'wechat';

        return $this;
    }

    public function sendEmail()
    {
        $this->path = 'email';

        return $this;
    }

    public function getPosts($slug)
    {
        $this->path = 'publishing/getPosts/'.$slug;

        return $this;
    }

}